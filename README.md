<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

## Installation

```bash
$ yarn
# or
$ yarn install
```

## Running the app

```bash
# development
$ yarn run start

# watch mode(hot reload)
$ yarn run start:dev

# production mode
$ yarn run start:prod
```

## Test

```bash
# unit tests
$ yarn run test

# e2e tests
$ yarn run test:e2e

# test coverage
$ yarn run test:cov
```

## Migration commands

```bash
# migrate DB
$ yarn run migrate <command [up|down]>
# example
$ yarn run migrate up

# create new migration file (new column)
$ yarn run mac <table-name> <column-name>

# create new migration file (change column)
$ yarn run mcc <table-name> <column-name>

# create new migration file (remove column)
$ yarn run mrc <table-name> <column-name>

# create new migration file (new table)
$ yarn run mct <table-name>
```

## Nest.js commands

```bash
# generate new compoent
$ nest generate  <component> <componet-name> modules

# example
$ nest generate mo user modules
```

#### components

| type       | command | description                       |
| ---------- | ------- | --------------------------------- |
| module     | mo      | Generate a module declaration     |
| class      | cl      | Generate a new class              |
| service    | s       | Generate a service declaration    |
| gateway    | ga      | Generate a gateway declaration    |
| guard      | gu      | Generate a guard declaration      |
| interface  | itf     | Generate an interface             |
| middleware | mi      | Generate a middleware declaration |
| pipe       | pi      | Generate a pipe declaration       |
