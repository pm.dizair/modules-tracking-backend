import { Sequelize } from "sequelize-typescript";
import { SequelizeStorage, Umzug } from "umzug";
import * as dotenv from "dotenv";
import { join } from "path";

dotenv.config();

const sequelize = new Sequelize({
  dialect: "postgres",
  host: process.env.POSTGRESSQL_DB_HOST,
  port: +process.env.POSTGRESSQL_DB_PORT,
  database: process.env.POSTGRESSQL_DB_NAME,
  username: process.env.POSTGRESSQL_DB_USERNAME,
  password: process.env.POSTGRESSQL_DB_PASSWORD,
});

export const migrator = new Umzug({
  migrations: {
    glob: [
      "migrations/[0-9]*.[0-9]*.[0-9]*T[0-9]*.[0-9]*.[0-9]*.[a-z_]*.ts",
      { cwd: join(__dirname, "../") },
    ],
  },
  storage: new SequelizeStorage({ sequelize }),
  context: sequelize.getQueryInterface(),
  logger: console,
});
