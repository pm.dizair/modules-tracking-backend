/* eslint-disable @typescript-eslint/no-var-requires */
require("ts-node/register");

const dotenv = require("dotenv");
switch (process.env.NODE_ENV) {
  case "dev": {
    console.log("LOADED DEV CONFIG");
    dotenv.config({ path: "./.env.dev" });
    break;
  }
  default: {
    console.log("LOADED LOCAL CONFIG");
    dotenv.config({ path: "./.env.local" });
  }
}

require("./migrator").migrator.runAsCLI();
