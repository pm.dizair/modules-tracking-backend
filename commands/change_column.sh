echo "import { DataTypes, QueryInterface, Sequelize } from \"sequelize\";
import { MigrationParams } from \"umzug\";

export const up = async ({
  context: queryInterface,
}: MigrationParams<QueryInterface>): Promise<void> => {
  await queryInterface.changeColumn(\"${1}\", \"${2}\", {});
};

export const down = async ({
  context: queryInterface,
}: MigrationParams<QueryInterface>): Promise<void> => {
  await queryInterface.changeColumn(\"${1}\", \"${2}\", {});
};" > migrations/$(date +%Y.%m.%dT%H.%M.%S).change_${1}_${2}_column.ts
