echo "import { DataTypes, QueryInterface, Sequelize } from \"sequelize\";
import { MigrationParams } from \"umzug\";

export const up = async ({
  context: queryInterface,
}: MigrationParams<QueryInterface>): Promise<void> => {
  await queryInterface.createTable(\"${1}\", {
    id: {
      primaryKey: true,
      allowNull: false,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
    createdAt: {
      type: DataTypes.DATE,
      defaultValue: Sequelize.literal(\"NOW()\"),
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: Sequelize.literal(\"NOW()\"),
    },
  });
};

export const down = async ({
  context: queryInterface,
}: MigrationParams<QueryInterface>): Promise<void> => {
  await queryInterface.dropTable(\"${1}\");
};" > migrations/$(date +%Y.%m.%dT%H.%M.%S).create_${1}_table.ts
