echo "import { DataTypes, QueryInterface, Sequelize } from \"sequelize\";
import { MigrationParams } from \"umzug\";

export const up = async ({
  context: queryInterface,
}: MigrationParams<QueryInterface>): Promise<void> => {
  await queryInterface.removeColumn(\"${1}\", \"${2}\");
};

export const down = async ({
  context: queryInterface,
}: MigrationParams<QueryInterface>): Promise<void> => {
  await queryInterface.addColumn(\"${1}\", \"${2}\", {});
};" > migrations/$(date +%Y.%m.%dT%H.%M.%S).remove_${1}_${2}_column.ts
