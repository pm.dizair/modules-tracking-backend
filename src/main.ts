import { Environment } from "@core/types/env";
import { BadRequestException, ValidationPipe } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { NestFactory } from "@nestjs/core";
import { ValidationError } from "class-validator";
import * as cookieParser from "cookie-parser";
import * as dotenv from "dotenv";
import { AppModule } from "./app.module";
import { NextFunction, Request } from "express";

const loadENV = () => {
  switch (process.env.NODE_ENV) {
    case "dev": {
      console.log("LOADED DEV CONFIG");
      dotenv.config({ path: "./.env.dev" });
      break;
    }
    default: {
      console.log("LOADED LOCAL CONFIG");
      dotenv.config({ path: "./.env.local" });
    }
  }
};

const bootstrap = async () => {
  const app = await NestFactory.create(AppModule);

  const configService = app.get(ConfigService<Environment>);

  app.use(cookieParser());
  app.enableCors({
    origin: String(configService.get("APP_CORS_ORIGIN_LIST")).split(", "),
    credentials: true,
  });

  app.setGlobalPrefix("api");
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
      exceptionFactory: (validationErrors: ValidationError[] = []) => {
        const parsedValidateErrors = validationErrors.map(
          ({ property, constraints }) => ({
            property,
            constraints,
          })
        );
        return new BadRequestException(parsedValidateErrors);
      },
    })
  );
  app.getHttpAdapter().getInstance().disable("x-powered-by");

  if (process.env.APP_DEBUG_MODE) {
    app.use((req: Request, _, next: NextFunction) => {
      console.log(req.url);
      next();
    });
  }

  await app.listen(configService.get("APP_PORT"));
};

loadENV();
bootstrap();
