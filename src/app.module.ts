@Module({
  imports: [
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: true,
      context: (context: GqlModuleOptions) => context,
    }),
    CacheModule.registerAsync<RedisClientOptions>({
      useClass: CacheConfigService,
      imports: [ConfigModule],
      inject: [ConfigService],
      isGlobal: true,
    }),
    ThrottlerModule.forRootAsync({
      useClass: ThrottleService,
    }),
    BullModule.forRootAsync({ useClass: BullConfigService }),
    ConfigModule.forRoot({ isGlobal: true }),
    MulterModule.register(),
    DatabaseModule,
    UserModule,
    TaskModule,    
  ],
  providers: [
    AppResolver,
    {
      provide: APP_GUARD,
      useClass: GqlThrottlerGuard,
    },
  ],
})
export class AppModule {}
