import { ROLE } from "@core/enums";
import { CompanyModel } from "@modules/company/company.model";
import {
  BelongsToManyCountAssociationsMixin,
  BelongsToManyGetAssociationsMixin,
  InferAttributes,
  InferCreationAttributes,
} from "sequelize";
import {
  BelongsTo,
  BelongsToMany,
  Column,
  DataType,
  Default,
  ForeignKey,
  HasMany,
  Model,
  PrimaryKey,
  Table,
} from "sequelize-typescript";
import { TaskModel, TaskUserModel } from "@modules/task/models";
import { UserModelAssociations, UserModelAttributes } from "./types";
import { ProjectModel } from "@modules/project/models/project";
import { ProjectUserModel } from "@modules/project/models/project-user";
import { SessionModel } from "@modules/session/session.model";

@Table({ tableName: "users" })
export class UserModel
  extends Model<InferAttributes<UserModel>, InferCreationAttributes<UserModel>>
  implements UserModelAttributes, UserModelAssociations
{
  //Columns
  @PrimaryKey
  @Default(DataType.UUIDV4)
  @Column
  id: string;

  @Column
  firstName: string;

  @Column
  lastName: string;

  @Column
  email: string;

  @Column
  avatarUrl: string;

  @Column
  role: ROLE;

  @Column
  password: string;

  @Column
  createdAt: Date;

  @Column
  updatedAt: Date;

  @ForeignKey(() => CompanyModel)
  companyId: string;

  //Associations
  @BelongsTo(() => CompanyModel)
  company: CompanyModel;

  @BelongsToMany(() => TaskModel, () => TaskUserModel)
  tasks: TaskModel[];

  @HasMany(() => SessionModel)
  sessions: SessionModel[];

  @BelongsToMany(() => ProjectModel, () => ProjectUserModel)
  projects: ProjectModel[];

  //Mixins
  getTasks: BelongsToManyGetAssociationsMixin<TaskModel>;
  getProjects: BelongsToManyGetAssociationsMixin<ProjectModel>;
  countProjects: BelongsToManyCountAssociationsMixin;
  countTasks: BelongsToManyCountAssociationsMixin;
}
