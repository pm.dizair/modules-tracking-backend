import { PaginationQueryParams } from "@core/types";

export interface GetUsersWithPaginationParams extends PaginationQueryParams {
  companyId: string;
}
