import { ROLE } from "@core/enums";
import { BaseModel } from "@core/types";
import { CompanyModel } from "@modules/company/company.model";
import { ProjectModel } from "@modules/project/models/project";
import { TaskModel } from "@modules/task/models";
import { SessionModel } from "@modules/session/session.model";

export interface UserModelAttributes extends BaseModel {
  firstName: string;
  lastName: string;
  avatarUrl: string;
  email: string;
  role: ROLE;
  password: string;
  companyId: string;
}

export interface UserModelAssociations {
  company: CompanyModel;
  tasks: TaskModel[];
  sessions: SessionModel[];
  projects: ProjectModel[];
}
