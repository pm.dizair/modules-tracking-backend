import { Global, Module } from "@nestjs/common";
import { UserService } from "./user.service";
import { userProviders } from "./user.providers";
import { UserResolver } from "./user.resolver";

@Global()
@Module({
  exports: [UserService],
  providers: [UserService, UserResolver, ...userProviders],
})
export class UserModule {}
