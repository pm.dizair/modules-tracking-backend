import { REPOSITORY } from "@core/enums";
import { UserModel } from "./user.model";
import { Provider } from "@nestjs/common";

export const userProviders: Provider[] = [
  {
    useValue: UserModel,
    provide: REPOSITORY.USER,
  },
];
