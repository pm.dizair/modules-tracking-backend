import { CompanyModel } from "@modules/company/company.model";
import { FindAttributeOptions, IncludeOptions } from "sequelize";

export const includeUserCompanyUtil: Pick<
  IncludeOptions,
  "attributes" | "include"
> = {
  attributes: { exclude: ["password"] },
  include: [CompanyModel],
};

export const includeUserAttributesUtil: FindAttributeOptions = {
  exclude: ["password"],
};
