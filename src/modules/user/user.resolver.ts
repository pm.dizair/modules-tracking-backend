import { ROLE } from "@core/enums";
import { AuthRoleRequest, GraphQLContext } from "@core/types";
import { JWTAuthGuard } from "@modules/auth/auth.guard";
import { Roles } from "@modules/auth/roles/roles.decorator";
import { RolesGuard } from "@modules/auth/roles/roles.guard";
import {
  BadRequestException,
  DefaultValuePipe,
  ForbiddenException,
  NotFoundException,
  ParseIntPipe,
  ParseUUIDPipe,
  UseGuards,
} from "@nestjs/common";
import {
  Args,
  Context,
  ID,
  Int,
  Mutation,
  Query,
  Resolver,
} from "@nestjs/graphql";
import * as bcrypt from "bcrypt";
import {
  CreateUserSchema,
  GetUsersWithPagination,
  UpdatePasswordSchema,
  UpdateUserRoleSchema,
  UserGraphQLSchema,
} from "./schemes";
import { UserService } from "./user.service";
import { getUnavailableRoles } from "@core/utils";
import { CreateCompanyAdminSchema } from "@modules/user/schemes";

@Resolver()
export class UserResolver {
  constructor(private readonly userService: UserService) {}
  @Query(() => GetUsersWithPagination, {
    description: "Returns paginated array of Users",
  })
  @UseGuards(JWTAuthGuard, RolesGuard)
  @Roles(ROLE.COMPANY_ADMIN)
  async getUsersWithPagination(
    @Context() ctx: GraphQLContext<AuthRoleRequest>,
    @Args(
      "page",
      { nullable: true, type: () => Int },
      new DefaultValuePipe(1),
      ParseIntPipe
    )
    page: number,
    @Args(
      "limit",
      { nullable: true, type: () => Int },
      new DefaultValuePipe(10),
      ParseIntPipe
    )
    limit: number
  ): Promise<GetUsersWithPagination> {
    const companyId = ctx.req.user.companyId;
    return await this.userService.getUsersWithPagination({
      companyId,
      limit,
      page,
    });
  }

  @UseGuards(JWTAuthGuard)
  @Query(() => UserGraphQLSchema, {
    description: "Takes user ID, returns User model",
  })
  async getUserInfo(
    @Context() ctx: GraphQLContext<AuthRoleRequest>,
    @Args("userId", { type: () => ID }, ParseUUIDPipe) userId: string
  ): Promise<UserGraphQLSchema> {
    const role = ctx.req.user.role;
    const user = await this.userService.getUserById(userId);

    if (!user) {
      throw new NotFoundException("User with this id was not found");
    }
    return {
      ...user.get(),
      role,
    };
  }

  @UseGuards(JWTAuthGuard, RolesGuard)
  @Roles(ROLE.SUPER_ADMIN)
  @Mutation(() => UserGraphQLSchema, {
    description: "Creates Company Admin user",
  })
  async createCompanyAdmin(
    @Args("schema") schema: CreateCompanyAdminSchema
  ): Promise<UserGraphQLSchema> {
    const { email, password } = schema;

    const oldUser = await this.userService.getUserByEmail(email);
    if (oldUser) {
      throw new BadRequestException("User already exists");
    }

    const hashPass = await bcrypt.hash(password, 10);

    return await this.userService.createUser({
      ...schema,
      password: hashPass,
      role: ROLE.COMPANY_ADMIN,
    });
  }

  @UseGuards(JWTAuthGuard, RolesGuard)
  @Roles(ROLE.COMPANY_ADMIN)
  @Mutation(() => UserGraphQLSchema, {
    description: "Creates user with given role",
  })
  async createUser(
    @Context() ctx: GraphQLContext<AuthRoleRequest>,
    @Args("schema") schema: CreateUserSchema
  ): Promise<UserGraphQLSchema> {
    const { role: userRole, companyId } = ctx.req.user;
    const { role, email, password } = schema;

    const unavailableRoles = getUnavailableRoles(userRole);

    if (unavailableRoles.includes(role)) {
      throw new ForbiddenException("Insufficient rights to set role");
    }

    const oldUser = await this.userService.getUserByEmail(email);
    if (oldUser) {
      throw new BadRequestException("User already exists");
    }

    const hashPass = await bcrypt.hash(password, 10);

    return await this.userService.createUser({
      ...schema,
      companyId,
      password: hashPass,
    });
  }

  @UseGuards(JWTAuthGuard)
  @Mutation(() => UserGraphQLSchema, { description: "Changes user password" })
  async changeUserPassword(
    @Context() ctx: GraphQLContext<AuthRoleRequest>,
    @Args("schema") schema: UpdatePasswordSchema
  ): Promise<UserGraphQLSchema> {
    const userId = ctx.req.user.userId;
    const { oldPassword, password } = schema;

    const isCompareNewOld = oldPassword === password;
    if (isCompareNewOld) {
      throw new BadRequestException(
        "New and old password should not be the same!"
      );
    }

    const currentPassword = await this.userService.getUserPassword(userId);
    const isCompare = await bcrypt.compare(oldPassword, currentPassword);
    if (!isCompare) {
      throw new BadRequestException("Old passwords are not comparing");
    }

    const hashPass = await bcrypt.hash(password, 10);
    return await this.userService.updateUser({
      userId,
      password: hashPass,
    });
  }

  @UseGuards(JWTAuthGuard, RolesGuard)
  @Roles(ROLE.COMPANY_ADMIN)
  @Mutation(() => UserGraphQLSchema, { description: "Changes user role" })
  async changeUserRole(
    @Context() ctx: GraphQLContext<AuthRoleRequest>,
    @Args("schema") schema: UpdateUserRoleSchema
  ): Promise<UserGraphQLSchema> {
    const currentUserId = ctx.req.user.userId;
    const { userId, role } = schema;

    if (userId === currentUserId) {
      throw new ForbiddenException(
        "You are trying to change the role of your account"
      );
    }

    if ([ROLE.SUPER_ADMIN, ROLE.COMPANY_ADMIN].includes(role)) {
      throw new ForbiddenException("Cannot set current role");
    }

    return await this.userService.updateUser({ userId, role });
  }

  @UseGuards(JWTAuthGuard, RolesGuard)
  @Roles(ROLE.COMPANY_ADMIN)
  @Mutation(() => Boolean, { nullable: true, description: "Removes user" })
  async removeUser(
    @Context() ctx: GraphQLContext<AuthRoleRequest>,
    @Args("userId", { type: () => ID }, ParseUUIDPipe) userId: string
  ): Promise<void> {
    const currentUserId = ctx.req.user.userId;
    if (userId === currentUserId) {
      throw new ForbiddenException("You are trying to delete your account");
    }

    await this.userService.removeUser(userId);
  }
}
