import { ROLE } from "@core/enums";
import { Field, InputType } from "@nestjs/graphql";
import {
  IsEmail,
  IsIn,
  IsNotEmpty,
  IsOptional,
  IsStrongPassword,
  MinLength,
} from "class-validator";
import { UserModelAttributes } from "../types";

@InputType()
export class CreateUserSchema implements Partial<UserModelAttributes> {
  @Field(() => String)
  @IsNotEmpty()
  @MinLength(2)
  firstName: string;

  @Field(() => String)
  @IsNotEmpty()
  @MinLength(2)
  lastName: string;

  @Field(() => String)
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @Field(() => String)
  @IsOptional()
  @IsIn(Object.values(ROLE))
  role: ROLE;

  @Field(() => String)
  @IsNotEmpty()
  @IsStrongPassword()
  password: string;
}
