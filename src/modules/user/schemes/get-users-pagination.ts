import { Field, ObjectType } from "@nestjs/graphql";
import { PaginationResult } from "@core/types";
import { UserGraphQLSchema } from "./user";
import { UserModel } from "../user.model";

@ObjectType()
export class GetUsersWithPagination implements PaginationResult {
  @Field({ nullable: true })
  count: number;

  @Field(() => [UserGraphQLSchema])
  rows: UserModel[];
}
