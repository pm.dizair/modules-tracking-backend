import { ROLE } from "@core/enums";
import { Field, ID, InputType } from "@nestjs/graphql";
import { IsIn, IsNotEmpty, IsUUID } from "class-validator";
import { UserModelAttributes } from "../types";

@InputType()
export class UpdateUserRoleSchema implements Partial<UserModelAttributes> {
  @Field(() => ID)
  @IsNotEmpty()
  @IsUUID()
  userId: string;

  @Field(() => String)
  @IsNotEmpty()
  @IsIn(Object.values(ROLE))
  role: ROLE;
}
