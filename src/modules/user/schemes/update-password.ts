import { Field, InputType } from "@nestjs/graphql";
import { IsNotEmpty, IsStrongPassword } from "class-validator";

@InputType()
export class UpdatePasswordSchema {
  @Field(() => String)
  @IsNotEmpty()
  oldPassword: string;

  @Field(() => String)
  @IsNotEmpty()
  @IsStrongPassword()
  password: string;
}
