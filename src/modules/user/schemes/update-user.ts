import { ROLE } from "@core/enums";
import { Field, ID } from "@nestjs/graphql";
import {
  IsIn,
  IsNotEmpty,
  IsOptional,
  IsStrongPassword,
  IsUUID,
  MinLength,
} from "class-validator";
import { UserModelAttributes } from "../types";

export class UpdateUserSchema implements Partial<UserModelAttributes> {
  @Field(() => ID)
  @IsNotEmpty()
  @IsUUID()
  userId: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  @IsIn(Object.values(ROLE))
  role?: ROLE;

  @Field(() => String, { nullable: true })
  @IsOptional()
  @MinLength(2)
  firstName?: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  @MinLength(2)
  lastName?: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  @IsStrongPassword()
  password?: string;
}
