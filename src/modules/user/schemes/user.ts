import { Field, ID, ObjectType } from "@nestjs/graphql";
import { UserModelAttributes } from "../types";
import { ROLE } from "@core/enums";

@ObjectType()
export class UserGraphQLSchema
  implements Omit<UserModelAttributes, "password">
{
  @Field(() => ID)
  id: string;

  @Field(() => String)
  firstName: string;

  @Field(() => String)
  lastName: string;

  @Field(() => String, { nullable: true })
  avatarUrl: string;

  @Field(() => String)
  email: string;

  @Field(() => String)
  role: ROLE;

  @Field(() => String, { nullable: true })
  companyId: string;

  @Field(() => Date)
  createdAt: Date;

  @Field(() => Date)
  updatedAt: Date;
}
