import {
  IsEmail,
  IsNotEmpty,
  IsStrongPassword,
  IsUUID,
  MinLength,
} from "class-validator";
import { UserModelAttributes } from "../types";
import { Field, ID, InputType } from "@nestjs/graphql";

@InputType()
export class CreateCompanyAdminSchema implements Partial<UserModelAttributes> {
  @Field(() => String)
  @IsNotEmpty()
  @MinLength(2)
  firstName: string;

  @Field(() => String)
  @IsNotEmpty()
  @MinLength(2)
  lastName: string;

  @Field(() => String)
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @Field(() => ID)
  @IsNotEmpty()
  @IsUUID()
  companyId: string;

  @Field(() => String)
  @IsNotEmpty()
  @IsStrongPassword()
  password: string;
}
