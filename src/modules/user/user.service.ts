import { Inject, Injectable, NotFoundException } from "@nestjs/common";
import { UserModel } from "./user.model";
import { includeUserAttributesUtil } from "./user.utils";
import { REPOSITORY } from "@core/enums";
import { getPagination } from "@core/utils";
import { CreateUserSchema, UpdateUserSchema } from "./schemes";
import { WithCompanyId } from "@core/types";
import { GetUsersWithPaginationParams } from "./types";

@Injectable()
export class UserService {
  constructor(
    @Inject(REPOSITORY.USER)
    private readonly userModel: typeof UserModel
  ) {}

  async getUserById(id: string) {
    const user = await this.userModel.findByPk(id, {
      attributes: includeUserAttributesUtil,
    });

    if (!user) {
      throw new NotFoundException(`User with ${id} not found`);
    }

    return user;
  }

  async getUserPassword(id: string) {
    const user = await this.userModel.findByPk(id);
    return user.password;
  }

  async getUsersByIds(ids: string[]) {
    return await this.userModel.findAll({
      where: { id: ids },
      attributes: includeUserAttributesUtil,
    });
  }

  async getUserByEmail(email: string) {
    return await this.userModel.findOne({ where: { email } });
  }

  async getUsersWithPagination({
    page,
    limit,
    companyId,
  }: GetUsersWithPaginationParams) {
    return await this.userModel.findAndCountAll({
      ...getPagination(page, limit),
      attributes: includeUserAttributesUtil,
      where: {
        companyId,
      },
    });
  }

  async createUser(schema: WithCompanyId<CreateUserSchema>) {
    const user = await this.userModel.create(schema);
    delete user.dataValues.password;
    return user;
  }

  async updateUser(schema: UpdateUserSchema) {
    const user = await this.userModel.findByPk(schema.userId);
    await user.update(schema);
    return user;
  }

  async removeUser(id: string) {
    return await this.userModel.destroy({ where: { id } });
  }
}
