import { Field, ID, ObjectType } from "@nestjs/graphql";
import { TaskModelAttributes } from "../types/model";
import { TASK_STATUS } from "@core/enums";

@ObjectType()
export class TaskGraphQLSchema implements TaskModelAttributes {
  @Field(() => ID)
  id: string;

  @Field(() => String)
  title: string;

  @Field(() => String)
  description: string;

  @Field(() => String)
  status: TASK_STATUS;

  @Field(() => [String], { nullable: true })
  fileUrls: string[];

  @Field(() => Boolean)
  isArchived: boolean;

  @Field(() => ID)
  projectId: string;

  @Field(() => ID)
  locationId: string;

  @Field(() => Date)
  updatedAt: Date;

  @Field(() => Date)
  createdAt: Date;
}
