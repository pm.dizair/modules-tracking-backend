import { LocationModel } from "@modules/location/location.model";
import { TaskModelAssociations } from "../types";
import { TaskGraphQLSchema } from "./task";
import { Field, ObjectType } from "@nestjs/graphql";
import { LocationGraphQLSchema } from "@modules/location/schemes";

@ObjectType()
export class TaskWithLocationGraphQLSchema
  extends TaskGraphQLSchema
  implements Partial<TaskModelAssociations>
{
  @Field(() => LocationGraphQLSchema)
  location: LocationModel;
}
