import { TaskModelAssociations } from "../types";
import { UserModel } from "@modules/user/user.model";
import { Field, ObjectType } from "@nestjs/graphql";
import { UserGraphQLSchema } from "@modules/user/schemes";
import { TaskGraphQLSchema } from "./task";
import { LocationModel } from "@modules/location/location.model";
import { LocationGraphQLSchema } from "@modules/location/schemes";

@ObjectType()
export class TaskDetailsGraphQLSchema
  extends TaskGraphQLSchema
  implements Partial<TaskModelAssociations>
{
  //Associations
  @Field(() => [UserGraphQLSchema])
  users: UserModel[];

  @Field(() => LocationGraphQLSchema)
  location: LocationModel;
}
