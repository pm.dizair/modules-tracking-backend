import { PaginationResult } from "@core/types";
import { Field, Int, ObjectType } from "@nestjs/graphql";
import { TaskModel } from "../models";
import { TaskWithLocationGraphQLSchema } from ".";

@ObjectType()
export class GetTasksWithPaginationSchema implements PaginationResult {
  @Field(() => Int)
  count: number;

  @Field(() => [TaskWithLocationGraphQLSchema])
  rows: TaskModel[];
}
