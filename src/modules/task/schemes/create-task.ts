import { Field, ID, InputType } from "@nestjs/graphql";
import {
  IsArray,
  IsIn,
  IsNotEmpty,
  IsOptional,
  IsUrl,
  IsUUID,
  MaxLength,
} from "class-validator";
import { TaskModelAttributes } from "../types/model";
import { TASK_STATUS } from "@core/enums";

@InputType()
export class CreateTaskSchema implements Partial<TaskModelAttributes> {
  @Field(() => String)
  @IsNotEmpty()
  @MaxLength(512)
  title: string;

  @Field(() => String)
  @IsOptional()
  description: string;

  @Field(() => String, { nullable: true })
  @IsOptional()
  @IsIn(Object.values(TASK_STATUS))
  status: TASK_STATUS;

  @Field(() => [String], { nullable: true })
  @IsOptional()
  @IsArray()
  @IsUUID(4, { each: true })
  userIds: string[];

  @Field(() => [String], { nullable: true })
  @IsOptional()
  @IsArray()
  @IsUrl({}, { each: true })
  fileUrls: string[];

  @Field(() => ID)
  @IsNotEmpty()
  projectId: string;
}
