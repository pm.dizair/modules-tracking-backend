import { LocationModel } from "@modules/location/location.model";
import { UserModel } from "@modules/user/user.model";
import { includeUserAttributesUtil } from "@modules/user/user.utils";
import { Includeable } from "sequelize";

export const taskDetailIncludeable: Includeable[] = [
  {
    model: UserModel,
    attributes: includeUserAttributesUtil,
    through: { attributes: [] },
  },
  LocationModel,
];

export const taskPaginationIncludeable: Includeable[] = [LocationModel];
