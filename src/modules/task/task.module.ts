import { Module } from "@nestjs/common";
import { taskProviders } from "./task.providers";
import { TaskService } from "./task.service";
import { TaskResolver } from "./task.resolver";
import { ProjectModule } from "@modules/project/project.module";
import { TaskController } from "./task.controller";
import { CloudinaryModule } from "@core/cloudinary/cloudinary.module";

@Module({
  controllers: [TaskController],
  imports: [ProjectModule, CloudinaryModule],
  providers: [TaskResolver, TaskService, ...taskProviders],
  exports: [TaskService],
})
export class TaskModule {}
