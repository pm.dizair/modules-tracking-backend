import { PaginationQueryParams } from "@core/types";

export interface GetTasksWithPaginationParams extends PaginationQueryParams {
  userId: string;
  projectId: string;
}
