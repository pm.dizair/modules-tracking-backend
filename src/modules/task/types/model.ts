import { TASK_STATUS } from "@core/enums";
import { BaseModel } from "@core/types";
import { LocationModel } from "@modules/location/location.model";
import { ProjectModel } from "@modules/project/models/project";
import { SessionModel } from "@modules/session/session.model";
import { UserModel } from "@modules/user/user.model";

export interface TaskModelAttributes extends BaseModel {
  title: string;
  description: string;
  status: TASK_STATUS;
  isArchived: boolean;
  fileUrls: string[];
  projectId: string;
  locationId: string;
}

export interface TaskModelAssociations {
  users: UserModel[];
  project: ProjectModel;
  session: SessionModel[];
  location: LocationModel;
}

export interface TaskUsersModelAttributes {
  taskId: string;
  userId: string;
}
