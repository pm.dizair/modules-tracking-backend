import { UserService } from "@modules/user/user.service";
import {
  BadRequestException,
  Inject,
  Injectable,
  NotFoundException,
} from "@nestjs/common";
import { TaskModel } from "./models/task";
import { taskDetailIncludeable } from "./task.utils";
import { REPOSITORY } from "@core/enums";
import { getPagination } from "@core/utils";
import { WithCompanyId } from "@core/types";
import { BelongsToManyGetAssociationsMixinOptions, Sequelize } from "sequelize";
import { UserModel } from "@modules/user/user.model";
import { ProjectService } from "@modules/project/project.service";
import { GetTasksWithPaginationParams } from "./types";
import { CreateTaskSchema, UpdateTaskSchema } from "./schemes";
import { CloudinaryService } from "@core/cloudinary/cloudinary.service";

@Injectable()
export class TaskService {
  constructor(
    @Inject(REPOSITORY.SEQUELIZE)
    private readonly sequelize: Sequelize,
    @Inject(REPOSITORY.TASK)
    private readonly taskModel: typeof TaskModel,
    private readonly userService: UserService,
    private readonly projectService: ProjectService,
    private readonly cloudinaryService: CloudinaryService
  ) {}

  async getTaskById(id: string): Promise<TaskModel> {
    const user = await this.taskModel.findByPk(id);

    if (!user) {
      throw new NotFoundException(`Task with ${id} not found`);
    }

    return user;
  }

  async getTaskDetails(taskId: string): Promise<TaskModel> {
    return await this.taskModel.findByPk(taskId, {
      include: taskDetailIncludeable,
    });
  }

  async getTasksWithPagination({
    page,
    limit,
    userId,
    projectId,
  }: GetTasksWithPaginationParams) {
    let options: BelongsToManyGetAssociationsMixinOptions = {
      ...getPagination(page, limit),
      include: taskDetailIncludeable,
    };

    if (projectId) {
      options = {
        ...options,
        where: { projectId },
      };
    }

    if (userId) {
      const user = await this.userService.getUserById(userId);

      const count = await user.countTasks({ where: options.where });
      const rows = await user.getTasks(options);

      return { count, rows };
    }
    return await this.taskModel.findAndCountAll(options);
  }

  async createTask(schema: WithCompanyId<CreateTaskSchema>) {
    const { title, companyId } = schema;
    const oldTask = await this.taskModel.findOne({
      where: { title, companyId },
    });

    if (oldTask) {
      //TODO!: Expand error types
      throw new BadRequestException();
    }

    return await this.sequelize.transaction(async (transaction) => {
      const task = await this.taskModel.create(schema, { transaction });

      if (schema.userIds) {
        const users = await this.userService.getUsersByIds(schema.userIds);
        await task.setUsers(users, { transaction });
      }

      if (schema.fileUrls) {
        //TODO!: Pin files
      }

      return task;
    });
  }

  async getUsersOfTask(taskId: string): Promise<UserModel[]> {
    const task = await this.getTaskById(taskId);
    return await task.getUsers();
  }

  async updateTask(schema: UpdateTaskSchema): Promise<TaskModel> {
    const task = await this.getTaskById(schema.id);
    await task.update(schema);
    return task;
  }

  async deleteTask(taskId: string): Promise<void> {
    const task = await this.getTaskById(taskId);
    const taskUsers = await task.getUsers();
    const arrayToRemove = taskUsers.map(({ id }) => id);
    await task.removeUsers(arrayToRemove);
    await task.destroy();
  }

  async setUsersToTask(
    taskId: string,
    userIds: string[]
  ): Promise<UserModel[]> {
    const task = await this.getTaskById(taskId);
    const projectUsers = await this.projectService.getProjectUsers(
      task.projectId
    );
    const projectUserIds = projectUsers.map(({ id }) => id);

    if (userIds.length !== projectUserIds.length) {
      throw new BadRequestException("User IDs arrays have invalid value");
    }

    await task.setUsers(userIds);

    return await task.getUsers();
  }

  async uploadFiles(
    id: string,
    files: Express.Multer.File[]
  ): Promise<TaskModel> {
    const task = await this.getTaskById(id);
    const uploadResult = await this.cloudinaryService.uploadFiles(files);
    if (uploadResult) {
      const oldUrls = task.fileUrls || [];
      const fileUrls = uploadResult.map(({ secure_url }) => secure_url);
      return await task.update({ id, fileUrls: oldUrls.concat(fileUrls) });
    }
  }

  async removeFiles(id: string, urls: string | string[]): Promise<TaskModel> {
    if (typeof urls === "string") {
      urls = [urls];
    }
    const task = await this.getTaskById(id);
    const removeResult = await this.cloudinaryService.removeFiles(urls);
    const urlsToDelete = [];
    const urlsError = [];

    urls.forEach((url, idx) =>
      removeResult[idx]["result"] === "ok"
        ? urlsToDelete.push(url)
        : urlsError.push(url)
    );

    const fileUrls = task.fileUrls.filter((url) => !urlsToDelete.includes(url));
    await task.update({ id, fileUrls });
    if (urlsError.length > 0) {
      throw new NotFoundException(
        `Images with urls ${urlsToDelete} succesfully deleted, but images with urls ${urlsError} not found`
      );
    }
    return task;
  }
}
