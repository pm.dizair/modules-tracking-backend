import { REPOSITORY } from "@core/enums";
import { Provider } from "@nestjs/common";
import { TaskModel } from "./models/task";

export const taskProviders: Provider[] = [
  {
    useValue: TaskModel,
    provide: REPOSITORY.TASK,
  },
];
