import { ROLE, TASK_STATUS } from "@core/enums";
import { AuthRoleRequest, GraphQLContext, PaginationResult } from "@core/types";
import { JWTAuthGuard } from "@modules/auth/auth.guard";
import { Roles } from "@modules/auth/roles/roles.decorator";
import { RolesGuard } from "@modules/auth/roles/roles.guard";
import { UserGraphQLSchema } from "@modules/user/schemes";
import { UserModel } from "@modules/user/user.model";
import {
  DefaultValuePipe,
  ParseEnumPipe,
  ParseIntPipe,
  ParseUUIDPipe,
  UseGuards,
} from "@nestjs/common";
import {
  Args,
  Context,
  ID,
  Int,
  Mutation,
  Query,
  Resolver,
} from "@nestjs/graphql";
import { TaskModel } from "./models/task";
import {
  CreateTaskSchema,
  GetTasksWithPaginationSchema,
  TaskDetailsGraphQLSchema,
  TaskGraphQLSchema,
  UpdateTaskSchema,
} from "./schemes";
import { TaskService } from "./task.service";

@Resolver()
export class TaskResolver {
  constructor(private readonly taskService: TaskService) {}

  @Query(() => TaskDetailsGraphQLSchema, {
    description: "Takes taskId, terurns Task",
  })
  async getTaskDetails(@Args("taskId") taskId: string): Promise<TaskModel> {
    return await this.taskService.getTaskDetails(taskId);
  }

  @Mutation(() => TaskGraphQLSchema, {
    description: "Creates task with given values",
  })
  @UseGuards(JWTAuthGuard, RolesGuard)
  @Roles(ROLE.COMPANY_ADMIN, ROLE.MANAGER)
  async createTask(
    @Context() ctx: GraphQLContext<AuthRoleRequest>,
    @Args("schema") schema: CreateTaskSchema
  ): Promise<TaskModel> {
    const companyId = ctx.req.user.companyId;

    return await this.taskService.createTask({
      ...schema,
      companyId,
    });
  }

  @Query(() => GetTasksWithPaginationSchema, {
    description: "Returns paginated array of Tasks ",
  })
  @UseGuards(JWTAuthGuard)
  async getTasksWithPagination(
    @Args(
      "page",
      { nullable: true, type: () => Int },
      new DefaultValuePipe(1),
      ParseIntPipe
    )
    page: number,
    @Args(
      "limit",
      { nullable: true, type: () => Int },
      new DefaultValuePipe(10),
      ParseIntPipe
    )
    limit: number,
    @Args(
      "userId",
      { nullable: true, type: () => ID },
      new ParseUUIDPipe({ optional: true })
    )
    userId: string,
    @Args(
      "projectId",
      { nullable: true, type: () => ID },
      new ParseUUIDPipe({ optional: true })
    )
    projectId: string
  ): Promise<PaginationResult<TaskModel>> {
    return await this.taskService.getTasksWithPagination({
      page,
      limit,
      userId,
      projectId,
    });
  }

  @Query(() => [UserGraphQLSchema], {
    description: "Returns array of users by taskId",
  })
  @UseGuards(JWTAuthGuard, RolesGuard)
  @Roles(ROLE.COMPANY_ADMIN, ROLE.MANAGER)
  async getUsersOfTask(
    @Args("taskId", { type: () => ID }, ParseUUIDPipe) taskId: string
  ): Promise<UserModel[]> {
    return await this.taskService.getUsersOfTask(taskId);
  }

  @Mutation(() => TaskGraphQLSchema, {
    description:
      "Updates task with given parameters. (For updating task status, please use 'updateTaskStatus' mutation)",
  })
  @UseGuards(JWTAuthGuard)
  @Roles(ROLE.COMPANY_ADMIN, ROLE.MANAGER)
  async updateTask(
    @Args("schema") schema: UpdateTaskSchema
  ): Promise<TaskModel> {
    return await this.taskService.updateTask(schema);
  }

  @Mutation(() => TaskGraphQLSchema, { description: "Updates task status" })
  @UseGuards(JWTAuthGuard, RolesGuard)
  @Roles(ROLE.COMPANY_ADMIN, ROLE.MANAGER)
  async updateTaskStatus(
    @Args("taskId", { type: () => ID }, ParseUUIDPipe) taskId: string,
    @Args("status", new ParseEnumPipe(TASK_STATUS)) status: TASK_STATUS
  ): Promise<TaskModel> {
    return await this.taskService.updateTask({ id: taskId, status });
  }

  @Mutation(() => TaskGraphQLSchema, {
    description: "Takes array of user ids and sets it to task",
  })
  @UseGuards(JWTAuthGuard, RolesGuard)
  @Roles(ROLE.COMPANY_ADMIN, ROLE.MANAGER)
  async setUsersToTask(
    @Args("taskId", { type: () => ID }, ParseUUIDPipe) taskId: string,
    @Args("userIds", { type: () => [ID] })
    userIds: string[]
  ): Promise<UserModel[]> {
    return await this.taskService.setUsersToTask(taskId, userIds);
  }

  @Mutation(() => TaskGraphQLSchema, {
    description: "Updates fileUrls of task",
  })
  @UseGuards(JWTAuthGuard, RolesGuard)
  @Roles(ROLE.COMPANY_ADMIN, ROLE.MANAGER)
  async updateTaskFiles(
    @Args("taskId", { type: () => ID }, ParseUUIDPipe) taskId: string,
    @Args("fileUrls", { type: () => [String] }) fileUrls: string[]
  ): Promise<TaskModel> {
    return await this.taskService.updateTask({ id: taskId, fileUrls });
  }

  @Mutation(() => Boolean, { nullable: true })
  @UseGuards(JWTAuthGuard, RolesGuard)
  @Roles(ROLE.COMPANY_ADMIN, ROLE.MANAGER)
  async deleteTask(
    @Args("taskId", { type: () => ID }, ParseUUIDPipe) taskId: string
  ): Promise<void> {
    return this.taskService.deleteTask(taskId);
  }
}
