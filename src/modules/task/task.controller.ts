import {
  Controller,
  Delete,
  ParseUUIDPipe,
  Post,
  Query,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
} from "@nestjs/common";
import { JWTAuthGuard } from "@modules/auth/auth.guard";
import { FilesInterceptor } from "@nestjs/platform-express";
import { TaskService } from "./task.service";
import { TaskModel } from "./models";
import { RolesGuard } from "@modules/auth/roles/roles.guard";
import { Roles } from "@modules/auth/roles/roles.decorator";
import { ROLE } from "@core/enums";

@Controller("task/files")
export class TaskController {
  constructor(private readonly taskService: TaskService) {}

  @Post("/upload")
  @UseGuards(JWTAuthGuard, RolesGuard)
  @Roles(ROLE.COMPANY_ADMIN, ROLE.MANAGER)
  @UseInterceptors(FilesInterceptor("files"))
  async uploadeFile(
    @UploadedFiles() files: Express.Multer.File[],
    @Query("taskId", ParseUUIDPipe) taskId: string
  ): Promise<TaskModel> {
    return await this.taskService.uploadFiles(taskId, files);
  }

  @Delete("/delete")
  @UseGuards(JWTAuthGuard, RolesGuard)
  @Roles(ROLE.COMPANY_ADMIN, ROLE.MANAGER)
  async deleteFile(
    @Query("fileUrls") fileUrls: string | string[],
    @Query("taskId", ParseUUIDPipe) taskId: string
  ): Promise<TaskModel> {
    return await this.taskService.removeFiles(taskId, fileUrls);
  }
}
