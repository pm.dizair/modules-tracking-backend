import { UserModel } from "@modules/user/user.model";
import { ForeignKey, Model, Table } from "sequelize-typescript";
import { TaskModel } from "./task";
import { InferAttributes, InferCreationAttributes } from "sequelize";
import { TaskUsersModelAttributes } from "../types/model";

@Table({ tableName: "tasks_users", createdAt: false, updatedAt: false })
export class TaskUserModel
  extends Model<
    InferAttributes<TaskUserModel>,
    InferCreationAttributes<TaskUserModel>
  >
  implements TaskUsersModelAttributes
{
  @ForeignKey(() => TaskModel)
  taskId: string;

  @ForeignKey(() => UserModel)
  userId: string;
}
