import { CompanyModel } from "@modules/company/company.model";
import { UserModel } from "@modules/user/user.model";
import {
  BelongsToManyGetAssociationsMixin,
  BelongsToManyRemoveAssociationsMixin,
  BelongsToManySetAssociationsMixin,
  InferAttributes,
  InferCreationAttributes,
} from "sequelize";
import {
  BelongsTo,
  BelongsToMany,
  Column,
  DataType,
  Default,
  ForeignKey,
  HasMany,
  Model,
  PrimaryKey,
  Table,
} from "sequelize-typescript";
import { TaskUserModel } from "./tasks-users";
import { TASK_STATUS } from "@core/enums";
import { TaskModelAssociations, TaskModelAttributes } from "../types/model";
import { ProjectModel } from "@modules/project/models/project";
import { SessionModel } from "@modules/session/session.model";
import { LocationModel } from "@modules/location/location.model";

@Table({ tableName: "tasks" })
export class TaskModel
  extends Model<InferAttributes<TaskModel>, InferCreationAttributes<TaskModel>>
  implements TaskModelAttributes, TaskModelAssociations
{
  //Columns
  @PrimaryKey
  @Default(DataType.UUIDV4)
  @Column
  id: string;

  @Column
  title: string;

  @Column
  description: string;

  @Column
  status: TASK_STATUS;

  @Column
  isArchived: boolean;

  @Column({
    type: DataType.ARRAY(DataType.TEXT),
  })
  fileUrls: string[];

  @ForeignKey(() => CompanyModel)
  companyId: string;

  @ForeignKey(() => ProjectModel)
  projectId: string;

  @ForeignKey(() => LocationModel)
  locationId: string;

  @Column
  createdAt: Date;

  @Column
  updatedAt: Date;

  //Associations
  @BelongsToMany(() => UserModel, () => TaskUserModel)
  users: UserModel[];

  @BelongsTo(() => CompanyModel)
  company: CompanyModel;

  @BelongsTo(() => ProjectModel)
  project: ProjectModel;

  @HasMany(() => SessionModel)
  session: SessionModel[];

  @BelongsTo(() => LocationModel)
  location: LocationModel;
  //Mixins
  setUsers: BelongsToManySetAssociationsMixin<UserModel, UserModel["id"]>;
  removeUsers: BelongsToManyRemoveAssociationsMixin<UserModel, UserModel["id"]>;
  getUsers: BelongsToManyGetAssociationsMixin<UserModel>;
}
